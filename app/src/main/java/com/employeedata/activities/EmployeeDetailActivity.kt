package com.employeedata.activities

import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import com.employeedata.R
import com.employeedata.databse
import com.employeedata.modal.Employees
import com.employeedata.modal.Payment
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_employeedetail.*
import java.text.SimpleDateFormat
import java.util.*
class EmployeeDetailActivity : AppCompatActivity() {
    var cal = Calendar.getInstance()
    var empid : String?=null
    var dateSetListener : DatePickerDialog.OnDateSetListener? = null
    private var mMessageListener: ValueEventListener? = null
   // val queryRef = FirebaseDatabase.getInstance().getReference("Employee")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_employeedetail)
        setSupportActionBar(myToolbar)
        getSupportActionBar()!!.setDisplayShowTitleEnabled(false);
        txtTitle.setText("Emplyee Detail")
        if (myToolbar != null) {
            myToolbar.setNavigationOnClickListener { finish() }
        }
        databse.getDBreference();
        empid = intent.extras.getString("employee") as String
        linAddpayment.setOnClickListener { showDialog() }
        linshowattendance.setOnClickListener {  val i = Intent(applicationContext, PaymentListActivity::class.java)
            i.putExtra("employee",empid)
            startActivity(i) }
        linshowpayment.setOnClickListener { val i = Intent(applicationContext, AttendanceListActivity::class.java)
            i.putExtra("employee",empid)
            startActivity(i) }


        val queryRef2 = FirebaseDatabase.getInstance().getReference("Employee").child(empid!!)
        getEmployedetail(empid!!,queryRef2)
    }


    fun getEmployedetail(empid: String, queryRef2: DatabaseReference) {
        queryRef2.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {
                //Log.d("key",p0.key)
                var employee = p0.getValue(Employees::class.java!!)

                    txtname.text = employee!!.name
                    txtnumber.text = employee!!.number
                    txtperday.text = employee!!.amount.toString()
                    txtbalance.text = employee!!.balance.toString()
                    txtaddress.text = employee!!.address.toString()
                    imgedit.setOnClickListener { val i = Intent(applicationContext, AddNewEmployeeActivity::class.java)
                        i.putExtra("employee",employee)
                        startActivity(i) }
                    imgdelete.setOnClickListener { showDeleteDialog(empid!!) }
                }




            override fun onCancelled(p0: DatabaseError) {

            }

        })
    }

        private fun showDialog() {
        var dialogs = Dialog(this@EmployeeDetailActivity)
        dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogs.setCancelable(true)
        dialogs.setContentView(R.layout.dialog_payment)
        dialogs.window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        dialogs.show()
        window!!.setBackgroundDrawableResource(R.drawable.top_rounded_white)
        val editDate = dialogs.findViewById(R.id.editdate) as EditText
        val editAmount = dialogs.findViewById(R.id.editamount) as EditText

            cal.get(Calendar.YEAR)
            cal.get(Calendar.MONTH)
            cal.get(Calendar.DAY_OF_MONTH)
            val myFormat = "dd/MM/yyyy" // mention the format you need
            val sdf = SimpleDateFormat(myFormat, Locale.US)
            var time: String = sdf.format(cal.getTime())
            editDate.setText(time)
        dateSetListener = object : DatePickerDialog.OnDateSetListener {
            override fun onDateSet(
                view: DatePicker, year: Int, monthOfYear: Int,
                dayOfMonth: Int
            ) {
                cal.set(Calendar.YEAR, year)
                cal.set(Calendar.MONTH, monthOfYear)
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                val myFormat = "dd/MM/yyyy" // mention the format you need
                val sdf = SimpleDateFormat(myFormat, Locale.US)
                var time: String = sdf.format(cal.time)
                editDate.setText(time)
            }
        }
        editDate.setOnClickListener { DatePickerDialog(
            this@EmployeeDetailActivity,
            dateSetListener,
            // set DatePickerDialog to point to today's date when it loads up
            cal.get(Calendar.YEAR),
            cal.get(Calendar.MONTH),
            cal.get(Calendar.DAY_OF_MONTH)
        ).show() }
        val yesBtn = dialogs.findViewById(R.id.btnpay) as Button
        yesBtn.setOnClickListener {
            if(editAmount.text.toString().isEmpty() && editAmount.text.toString().equals("")){
                Toast.makeText(applicationContext, "Enter Amount", Toast.LENGTH_SHORT).show()
            }else{
                val date = SimpleDateFormat("dd/MM/yyyy").parse(editDate.text.toString())
                val user = Payment(empid, date.time.toString(),editAmount.text.toString().toInt(),"")
                databse.database.child("Payment").push().setValue(user).addOnSuccessListener {

                    FirebaseDatabase.getInstance().getReference("Employee").child(this!!.empid!!).child("balance").setValue(txtbalance.text.toString().toLong() - editAmount.text.toString().toLong()).addOnSuccessListener {
                        Toast.makeText(applicationContext, "balance updated..", Toast.LENGTH_SHORT).show()
                        getEmployedetail(this!!.empid!!, FirebaseDatabase.getInstance().getReference("Employee").child(this!!.empid!!))
                        //  queryRef.removeEventListener(this!!.mMessageListener!!)
                    }
                }
                    .addOnFailureListener {
                        Toast.makeText(applicationContext, "failde", Toast.LENGTH_SHORT).show()
                    }
                dialogs.dismiss()
            }

        }

    }
    fun showDeleteDialog(empid: String) {
        // Initialize a new instance of
        val builder = AlertDialog.Builder(this@EmployeeDetailActivity)

        // Set the alert dialog title
        builder.setTitle("Delete")

        // Display a message on alert dialog
        builder.setMessage("Are you want to sure to? delete ")

        // Set a positive button and its click listener on alert dialog
        builder.setPositiveButton("YES"){dialog, which ->
            // Do something when user press the positive button

            FirebaseDatabase.getInstance().getReference("Employee").child(empid).removeValue()
            finish()

        }


        // Display a negative button on alert dialog
        builder.setNegativeButton("Cancel"){dialog,which ->
            dialog.dismiss()
        }
        // Finally, make the alert dialog using builder
        val dialog: AlertDialog = builder.create()

        // Display the alert dialog on app interface
        dialog.show()
    }
    override fun onResume() {
        val queryRef2 = FirebaseDatabase.getInstance().getReference("Employee").child(empid!!)
        getEmployedetail(empid!!,queryRef2)
        super.onResume()
    }
}
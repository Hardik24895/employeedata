package com.employeedata.activities

import android.app.DatePickerDialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.MotionEvent
import android.view.View
import android.widget.DatePicker
import android.widget.LinearLayout
import android.widget.Toast
import com.employeedata.R
import com.employeedata.adapters.AttendanceEmployeeListAdapter
import com.employeedata.databse
import com.employeedata.modal.Attendance
import com.employeedata.modal.Employees
import com.employeedata.utility.constant
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_attendance.*
import java.text.SimpleDateFormat
import java.util.*
import android.view.WindowManager
import android.widget.TextView
import android.view.Gravity
import android.view.ViewGroup
import android.widget.ProgressBar
import android.graphics.Color
import android.support.v7.app.AlertDialog


class AddAttendanceActivity : AppCompatActivity() {
    var cal = Calendar.getInstance()
    private var mMessageListener: ValueEventListener? = null
    var dialog : AlertDialog? =null
    val queryRef = FirebaseDatabase.getInstance().getReference("Employee")
    val employeeList = ArrayList<Employees>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_attendance)
        constant.PreferenceManager(this@AddAttendanceActivity)
        databse.getDBreference();
        reclerview2.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false) as RecyclerView.LayoutManager?
      //  var mToolbar = findViewById(R.id.myToolbar) as Toolbar?
        setSupportActionBar(myToolbar)
            getSupportActionBar()!!.setDisplayShowTitleEnabled(false);
        txtTitle.setText("Add Attendance")
        if (myToolbar != null) {
            myToolbar.setNavigationOnClickListener { finish() }
        }
        getEmployeeList()
        cal.get(Calendar.YEAR)
        cal.get(Calendar.MONTH)
        cal.get(Calendar.DAY_OF_MONTH)
        val myFormat = "dd/MM/yyyy" // mention the format you need
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        var time: String = sdf.format(cal.getTime())
        editdate.setText(time)
        val dateSetListener = object : DatePickerDialog.OnDateSetListener {
            override fun onDateSet(
                view: DatePicker, year: Int, monthOfYear: Int,
                dayOfMonth: Int
            ) {
                cal.set(Calendar.YEAR, year)
                cal.set(Calendar.MONTH, monthOfYear)
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                val myFormat = "dd/MM/yyyy" // mention the format you need
                val sdf = SimpleDateFormat(myFormat, Locale.US)
                var time: String = sdf.format(cal.getTime())
                editdate.setText(time)
            }
        }
        editdate.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                when (event?.action) {
                    MotionEvent.ACTION_DOWN -> DatePickerDialog(
                        this@AddAttendanceActivity,
                        dateSetListener,
                        // set DatePickerDialog to point to today's date when it loads up
                        cal.get(Calendar.YEAR),
                        cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH)
                    ).show()
                }

                return v?.onTouchEvent(event) ?: true
            }
        })
    }

    fun getEmployeeList() {
        mMessageListener = queryRef!!.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {
                if (employeeList != null) {
                    employeeList.clear()
                }
                for (postSnapshot in p0.getChildren()) {
                    val employee = postSnapshot.getValue(Employees::class.java!!)

                    if (employee != null) {
                        if (employeeList != null) {
                            if (constant.getStringPreference("userkey").equals(employee.userid)) {
                                employee.empid= postSnapshot.key!!
                                employeeList.add(employee)
                                reclerview2.adapter?.notifyDataSetChanged()
                            }

                        }
                    }

                }

            }

            override fun onCancelled(p0: DatabaseError) {

            }

        })
        reclerview2.adapter = AttendanceEmployeeListAdapter(employeeList!!, { employee : Employees -> RightItemClicked(employee) },{ employee : Employees -> LeftItemClicked(employee) })

    }

    private fun RightItemClicked(employee: Employees) {
        setProgressDialog()
        dialog!!.show()
        val date = SimpleDateFormat("dd/MM/yyyy").parse(editdate.text.toString())
        val user = Attendance(employee.empid, date.time.toString(),"true")
        databse.database.child("Attendance").push().setValue(user).addOnSuccessListener {
            queryRef.child(employee.empid).child("balance").setValue(employee.balance + employee.amount)
            queryRef.removeEventListener(mMessageListener!!)
            employeeList.remove(employee)
            reclerview2.adapter!!.notifyDataSetChanged()
            dialog!!.hide()
        }
            .addOnFailureListener {
                dialog!!.hide()
                Toast.makeText(applicationContext, "failde", Toast.LENGTH_SHORT).show()
            }

    }


    private fun LeftItemClicked(employee: Employees) {
        employeeList.remove(employee)
        reclerview2.adapter!!.notifyDataSetChanged()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mMessageListener != null) {
            queryRef!!.removeEventListener(mMessageListener!!)
        }
    }

    fun setProgressDialog() {

        val llPadding = 30
        val ll = LinearLayout(this)
        ll.orientation = LinearLayout.HORIZONTAL
        ll.setPadding(llPadding, llPadding, llPadding, llPadding)
        ll.gravity = Gravity.CENTER
        var llParam = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        llParam.gravity = Gravity.CENTER
        ll.layoutParams = llParam

        val progressBar = ProgressBar(this)
        progressBar.isIndeterminate = true
        progressBar.setPadding(0, 0, llPadding, 0)
        progressBar.layoutParams = llParam

        llParam = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        llParam.gravity = Gravity.CENTER
        val tvText = TextView(this)
        tvText.text = "Loading ..."
        tvText.setTextColor(resources.getColor(R.color.media))
        tvText.textSize = 20f
        tvText.layoutParams = llParam

        ll.addView(progressBar)
        ll.addView(tvText)

        val builder = AlertDialog.Builder(this)
        builder.setCancelable(true)
        builder.setView(ll)

       dialog= builder.create()
        val window = dialog!!.getWindow()
        if (window != null) {
            val layoutParams = WindowManager.LayoutParams()
            layoutParams.copyFrom(dialog!!.getWindow().getAttributes())
            layoutParams.width = LinearLayout.LayoutParams.WRAP_CONTENT
            layoutParams.height = LinearLayout.LayoutParams.WRAP_CONTENT
            dialog!!.getWindow().setAttributes(layoutParams)
        }
    }

    }



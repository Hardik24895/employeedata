package com.employeedata.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.employeedata.R
import com.employeedata.utility.constant
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        constant.PreferenceManager(this@HomeActivity)
        hideSystemUI()
        linaddperson.setOnClickListener {
            val intent = Intent(applicationContext, AddNewEmployeeActivity::class.java)
            startActivity(intent) }
        linattendance.setOnClickListener {
            val intent = Intent(applicationContext, AddAttendanceActivity::class.java)
            startActivity(intent) }
        linviewperson.setOnClickListener {
            val intent = Intent(applicationContext, EmployeeListActivity::class.java)
            startActivity(intent) }
        imgprofile.setOnClickListener {
            val intent = Intent(applicationContext, ProfileActivity::class.java)
            startActivity(intent) }
        imglogout.setOnClickListener { showLogoutDialog() }
    }
    private fun hideSystemUI() {
        val decorView = window.decorView
        decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_IMMERSIVE
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                // Hide the nav bar and status bar
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN)
    }

    fun showLogoutDialog() {
        // Initialize a new instance of
        val builder = AlertDialog.Builder(this@HomeActivity)

        // Set the alert dialog title
        builder.setTitle("Logout")

        // Display a message on alert dialog
        builder.setMessage("Are you want to Sure?")

        // Set a positive button and its click listener on alert dialog
        builder.setPositiveButton("YES"){dialog, which ->
            // Do something when user press the positive button
            constant.clearPrefrences()
            val intent = Intent(applicationContext, LoginActivity::class.java)
            startActivity(intent)
            finish()

        }


        // Display a negative button on alert dialog
        builder.setNegativeButton("Cancel"){dialog,which ->
            dialog.dismiss()
        }
        // Finally, make the alert dialog using builder
        val dialog: AlertDialog = builder.create()

        // Display the alert dialog on app interface
        dialog.show()
    }
}
package com.employeedata.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.LinearLayout
import com.employeedata.R
import com.employeedata.adapters.AttendanceListAdapter
import com.employeedata.modal.Attendance
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_attendancelist.*
import java.util.*

class AttendanceListActivity : AppCompatActivity() {
    private var mMessageListener: ValueEventListener? = null
    var empid : String?=null
    val queryRef =   FirebaseDatabase.getInstance().getReference("Attendance")
    val attendanceList = ArrayList<Attendance>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_attendancelist)
        setSupportActionBar(myToolbar)
        getSupportActionBar()!!.setDisplayShowTitleEnabled(false);
        txtTitle.setText("Attendance List")
        if (myToolbar != null) {
            myToolbar.setNavigationOnClickListener { finish() }
        }
        reclerview6.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false) as RecyclerView.LayoutManager?
        empid = intent.extras.getString("employee") as String
        getPaymentList()
    }
    fun getPaymentList() {
        mMessageListener=  queryRef!!.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {
                if (attendanceList != null) {
                    attendanceList.clear()
                }
                for (postSnapshot in p0.getChildren()) {
                    val attendance = postSnapshot.getValue(Attendance::class.java!!)
                    if (attendance != null) {
                        if (attendanceList != null) {
                            if(empid.equals(attendance!!.empid)){
                                attendanceList.add(attendance)
                                reclerview6.adapter?.notifyDataSetChanged()
                            }

                        }
                    }
                }
            }

            override fun onCancelled(p0: DatabaseError) {

            }

        })
        reclerview6.adapter = AttendanceListAdapter(attendanceList!!)
    }




    override fun onStop() {
        super.onStop()
        if (mMessageListener != null) {
            queryRef!!.removeEventListener(mMessageListener!!)
        }
    }
}
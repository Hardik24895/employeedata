package com.employeedata.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.employeedata.R
import com.employeedata.databse
import com.employeedata.modal.Owners
import com.employeedata.utility.constant
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_signup.*
import com.google.firebase.database.DataSnapshot


class SignupActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        databse.getDBreference();
        hideSystemUI()
        btnsignup.setOnClickListener {
            if (editname.text.toString().isEmpty() && editname.text.toString().equals("")) {
                Toast.makeText(applicationContext, "Enter full Name", Toast.LENGTH_SHORT).show()
            } else if (editemail.text.toString().isEmpty() && editemail.text.toString().equals("")) {
                Toast.makeText(applicationContext, "Enter Email", Toast.LENGTH_SHORT).show()
            } else if (editbusiness.text.toString().isEmpty() && editbusiness.text.toString().equals("")) {
                Toast.makeText(applicationContext, "Enter business", Toast.LENGTH_SHORT).show()
            } else if (editnumber.text.toString().isEmpty() && editnumber.text.toString().equals("")) {
                Toast.makeText(applicationContext, "Enter Number", Toast.LENGTH_SHORT).show()
            } else if (editpassword.text.toString().isEmpty() && editpassword.text.toString().equals("")) {
                Toast.makeText(applicationContext, "Enter Password", Toast.LENGTH_SHORT).show()
            }  else {
                writeNewUser(
                    editname.text.toString(),
                    editemail.text.toString(),
                    editbusiness.text.toString(),
                    editnumber.text.toString(),
                    editpassword.text.toString()
                )
            }

        }
        txtlogin.setOnClickListener {
            val intent = Intent(applicationContext, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }
        val Owners = FirebaseDatabase.getInstance().getReference("Owners")
        Owners.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (messageSnapshot in dataSnapshot.children) {
                    val name = messageSnapshot.child("name").value as String?
                    val message = messageSnapshot.child("message").value as String?
                }

                // Log.d("values", "Value is: map.")
            }

            override fun onCancelled(databaseError: DatabaseError) {
                Log.w("value", "onCancelled", databaseError.toException())
            }
        })
    }

    private fun writeNewUser(name: String, email: String, business: String?, number: String, password: String) {
        val user = Owners(name, email, business, number, password)
        databse.database.child("Owners").push().setValue(user).addOnSuccessListener {
            Toast.makeText(applicationContext, "done", Toast.LENGTH_SHORT).show()
            val i = Intent(applicationContext, LoginActivity::class.java)
            startActivity(i)
            finish()

        }
            .addOnFailureListener {
                Toast.makeText(applicationContext, "failde", Toast.LENGTH_SHORT).show()
            }

    }

    private fun hideSystemUI() {
        val decorView = window.decorView
        decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_IMMERSIVE
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                // Hide the nav bar and status bar
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN)
    }

}
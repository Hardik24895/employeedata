package com.employeedata.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.employeedata.R
import com.employeedata.databse
import com.employeedata.modal.Employees
import com.employeedata.modal.Owners
import com.employeedata.utility.constant
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_profile.*

class ProfileActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        databse.getDBreference();
        setSupportActionBar(myToolbar)
        getSupportActionBar()!!.setDisplayShowTitleEnabled(false);
        txtTitle.setText("Profile Detail")
        if (myToolbar != null) {
            myToolbar.setNavigationOnClickListener { finish() }
        }
        getOwnerDetail()
    }

    fun getOwnerDetail() {
        val queryRef = FirebaseDatabase.getInstance().getReference("Owners").child(constant.getStringPreference("userkey"))
        queryRef.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {

                     val  owner = p0.getValue(Owners::class.java!!)

                      txtname.text = owner!!.name
                        txtnumber.text = owner!!.number
                        txtemail.text = owner!!.email.toString()
                        txtbusiness.text = owner!!.business.toString()




            }

            override fun onCancelled(p0: DatabaseError) {

            }

        })
    }
}
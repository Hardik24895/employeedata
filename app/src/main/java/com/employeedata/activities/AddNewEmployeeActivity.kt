package com.employeedata.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.employeedata.R
import com.employeedata.databse
import com.employeedata.modal.Employees
import com.employeedata.utility.constant
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_addemployee.*


class AddNewEmployeeActivity : AppCompatActivity() {
    var employees : Employees?=null
    val queryRef = FirebaseDatabase.getInstance().getReference("Employee")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_addemployee)
        databse.getDBreference();
        setSupportActionBar(myToolbar)
        getSupportActionBar()!!.setDisplayShowTitleEnabled(false);
        if (myToolbar != null) {
            myToolbar.setNavigationOnClickListener { finish() }
        }
        if(intent.hasExtra("employee")){
            txtTitle.setText("Edit Employee")
            employees = intent.extras.getSerializable("employee") as Employees
            btnaddemployee.setText("Save")
            editname.setText(employees!!.name)
            ediaddress.setText(employees!!.address)
            editnumber.setText(employees!!.number)
            editamount.setText(employees!!.amount.toString())
            editname.setSelection(editname.text.length)
            ediaddress.setSelection(ediaddress.text.length)
            editnumber.setSelection(editnumber.text.length)
            editamount.setSelection(editamount.text.length)
            btnaddemployee.setOnClickListener {
                val employees1 = Employees(editname.text.toString(),  editnumber.text.toString(),  ediaddress.text.toString(),    editamount.text.toString().toLong(),
                    employees!!.balance,0,constant.getStringPreference("userkey"),
                    employees!!.empid)
                queryRef.child(employees!!.empid).setValue(employees1).addOnSuccessListener {
                    finish()
                }

            }

        }else{
            txtTitle.setText("Add Employee")
            btnaddemployee.setOnClickListener {
                if(editname.text.toString().isEmpty()&& editname.text.toString().equals("")){
                    Toast.makeText(applicationContext, "Enter full Name", Toast.LENGTH_SHORT).show()
                }else if(editnumber.text.toString().isEmpty()&& editnumber.text.toString().equals("")) {
                    Toast.makeText(applicationContext, "Enter Number", Toast.LENGTH_SHORT).show()
                }
                else if(ediaddress.text.toString().isEmpty()&& ediaddress.text.toString().equals("")) {
                    Toast.makeText(applicationContext, "Enter Address", Toast.LENGTH_SHORT).show()
                }else if(editamount.text.toString().isEmpty()&& editamount.text.toString().equals("")) {
                    Toast.makeText(applicationContext, "Enter Amount", Toast.LENGTH_SHORT).show()
                }else{
                    writeNewEmployee(editname.text.toString(),
                        ediaddress.text.toString(),
                        editnumber.text.toString(),
                        editamount.text.toString().toLong())
                }

            }
        }

    }
    private fun writeNewEmployee(name: String,  address: String?, number: String, amount: Long) {
        val employees = Employees(name, number, address, amount,0,0,constant.getStringPreference("userkey"))
        databse.database.child("Employee").push().setValue(employees).addOnSuccessListener {
            Toast.makeText(applicationContext, "employee add", Toast.LENGTH_SHORT).show()
            val i = Intent(applicationContext, EmployeeListActivity::class.java)
            startActivity(i)
            finish()
        }.addOnFailureListener {
                Toast.makeText(applicationContext, "failde", Toast.LENGTH_SHORT).show()
            }
    }
}
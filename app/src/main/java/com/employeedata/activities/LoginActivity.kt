package com.employeedata.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Toast
import com.employeedata.R
import com.employeedata.modal.Owners
import com.employeedata.utility.constant
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {
    val queryRef = FirebaseDatabase.getInstance().getReference("Owners")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        hideSystemUI()
        constant.PreferenceManager(this@LoginActivity)
        btnlogin.setOnClickListener {
            if (editemail.text.toString().isEmpty() && editemail.text.toString().equals("")) {
                Toast.makeText(applicationContext, "Enter Email", Toast.LENGTH_SHORT).show()
            } else if (editpassword.text.toString().isEmpty() && editpassword.text.toString().equals("")) {
                Toast.makeText(applicationContext, "Enter Password", Toast.LENGTH_SHORT).show()
            } else {
                getUserKey(editemail.text.toString());
            }

        }
        txtsignup.setOnClickListener {
            val intent = Intent(applicationContext, SignupActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    fun getUserKey(email: String) {
        queryRef.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {
                //Log.d("key",p0.key)
                for (postSnapshot in p0.getChildren()) {
                    val owners = postSnapshot.getValue(Owners::class.java!!)
                    if (owners != null) {
                        if (email.equals(owners?.email) && editpassword.text.toString().equals(owners.password)) {
                            Log.d("key", postSnapshot.key)
                            constant.setBooleanPreference("login", true)
                            constant.USER_KEY = postSnapshot.key!!
                            constant.setStringPreference("userkey", postSnapshot.key!!)
                            Toast.makeText(applicationContext, "Login sucessfully", Toast.LENGTH_SHORT).show()
                            val i = Intent(applicationContext, HomeActivity::class.java)
                            startActivity(i)
                            finish()
                        } else {
                            // Toast.makeText(applicationContext, "Login faild", Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }

            override fun onCancelled(p0: DatabaseError) {

            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    private fun hideSystemUI() {
        val decorView = window.decorView
        decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_IMMERSIVE
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                // Hide the nav bar and status bar
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN)
    }
}



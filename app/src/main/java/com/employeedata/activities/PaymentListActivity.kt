package com.employeedata.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.LinearLayout
import com.employeedata.R
import com.employeedata.adapters.EmployeeListAdapter
import com.employeedata.adapters.PaymentListAdapter
import com.employeedata.modal.Employees
import com.employeedata.modal.Payment
import com.employeedata.utility.constant
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_paymentlist.*

class PaymentListActivity:AppCompatActivity() {
    private var mMessageListener: ValueEventListener? = null
    var empid : String?=null
    val queryRef =   FirebaseDatabase.getInstance().getReference("Payment")
    val paymentList = ArrayList<Payment>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_paymentlist)
        setSupportActionBar(myToolbar)
        getSupportActionBar()!!.setDisplayShowTitleEnabled(false);
        txtTitle.setText("Payment History")
        if (myToolbar != null) {
            myToolbar.setNavigationOnClickListener { finish() }
        }
        reclerview5.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false) as RecyclerView.LayoutManager?
        empid = intent.extras.getString("employee") as String
        getPaymentList()
    }
    fun getPaymentList() {
        mMessageListener=  queryRef!!.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {
                if (paymentList != null) {
                    paymentList.clear()
                }
                for (postSnapshot in p0.getChildren()) {
                    val payment = postSnapshot.getValue(Payment::class.java!!)

                    if (payment != null) {
                        if (paymentList != null) {
                            if(empid.equals(payment.empid)){
                                payment.payid= postSnapshot.key!!
                                queryRef.child(payment.payid!!).child("payid").setValue(payment.payid!!).addOnSuccessListener {
                                }
                                paymentList.add(payment)
                                reclerview5.adapter?.notifyDataSetChanged()
                            }

                        }
                    }


                }

            }

            override fun onCancelled(p0: DatabaseError) {

            }

        })
        reclerview5.adapter = PaymentListAdapter(paymentList!!)

    }



    override fun onStop() {
        super.onStop()

        if (mMessageListener != null) {
            queryRef!!.removeEventListener(mMessageListener!!)
        }
    }

}
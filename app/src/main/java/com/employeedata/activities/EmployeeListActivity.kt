package com.employeedata.activities

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.widget.LinearLayout
import android.widget.Toast
import com.employeedata.R
import com.employeedata.adapters.EmployeeListAdapter
import com.employeedata.databse
import com.employeedata.modal.Employees
import com.employeedata.utility.constant
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_employeelist.*
import com.google.firebase.database.DataSnapshot
import android.net.Uri
import android.support.v4.app.ActivityCompat
import java.util.*


class EmployeeListActivity : AppCompatActivity(){

    private var mMessageListener: ValueEventListener? = null
    val queryRef =   FirebaseDatabase.getInstance().getReference("Employee")
    val employeeList = ArrayList<Employees>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_employeelist)
        reclerview.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false) as RecyclerView.LayoutManager?
        constant.PreferenceManager(this@EmployeeListActivity)
        setSupportActionBar(myToolbar)
        getSupportActionBar()!!.setDisplayShowTitleEnabled(false);
        txtTitle.setText("Emplyee List")
        if (myToolbar != null) {
            myToolbar.setNavigationOnClickListener { finish() }
        }
       // databse.getDBreference();

        if (mMessageListener != null) {
            queryRef!!.addValueEventListener(mMessageListener!!)
        }
        getEmployeeList()

    }

    fun getEmployeeList() {

        mMessageListener=  queryRef!!.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {
                if (employeeList != null) {
                    employeeList.clear()
                }
                for (postSnapshot in p0.getChildren()) {
                    val employee = postSnapshot.getValue(Employees::class.java!!)

                    if (employee != null) {
                        if (employeeList != null) {
                            if(constant.getStringPreference("userkey").equals(employee.userid)){
                               employee.empid= postSnapshot.key!!
                                queryRef.child(employee.empid).child("empid").setValue(employee.empid).addOnSuccessListener {
                                }
                                employeeList.add(employee)
                                reclerview.adapter?.notifyDataSetChanged()
                            }

                        }
                    }


                }

            }

            override fun onCancelled(p0: DatabaseError) {

            }

        })
        reclerview.adapter = EmployeeListAdapter(employeeList!!,{ employee : Employees -> callclick(employee) },{ employee : Employees -> reportclick(employee) })

    }

    private fun reportclick(employee: Employees) {

        val i = Intent(applicationContext, EmployeeDetailActivity::class.java)
        i.putExtra("employee",employee.empid)
        startActivity(i)

    }

    private fun callclick(employee: Employees) {
        val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:" + employee.number))
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this as Activity,
                    Manifest.permission.CALL_PHONE)) {

            } else {
                ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.CALL_PHONE),
                    1)
            }
        }else{
            startActivity(intent)
        }


    }

    /* private fun employeeItemClicked(employee: Employees) {
          val i = Intent(applicationContext, EmployeeDetailActivity::class.java)
         i.putExtra("employee",employee.empid)
         startActivity(i)

     }*/

    override fun onStop() {
        super.onStop()

        /*if (mMessageListener != null) {
            queryRef!!.removeEventListener(mMessageListener!!)
        }*/
    }

    override fun onDestroy() {
        if (mMessageListener != null) {
           queryRef!!.removeEventListener(mMessageListener!!)
       }
        super.onDestroy()
    }
    override fun onResume() {
        //getEmployeeList()
        super.onResume()
    }

    }

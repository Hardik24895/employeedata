package com.employeedata.modal

import com.google.firebase.database.IgnoreExtraProperties
import java.io.Serializable

@IgnoreExtraProperties
data class Employees (
    var name: String? = "",
    var number: String? = "",
    var address: String? = "",
    var amount: Long =0,
    var balance: Long =0,
    var withdraw: Long =0,
    val userid: String ="",
    var empid: String =""
):Serializable
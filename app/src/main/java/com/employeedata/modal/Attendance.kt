package com.employeedata.modal

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
data class Attendance (
    var empid: String? = "",
    var date: String? = "",
    var ispresent: String? = ""
)
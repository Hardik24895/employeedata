package com.employeedata.modal

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
data class Owners(
    var name: String? = "",
    var email: String? = "",
    var business: String? = "",
    var number: String? = "",
    var password: String? = ""

)
package com.employeedata.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.employeedata.R
import com.employeedata.modal.Employees
import kotlinx.android.synthetic.main.row_empllist.view.*

class AttendanceEmployeeListAdapter  (
    val employeeList: ArrayList<Employees>,
    val RightclickListener: (Employees) -> Unit,
    val LeftclickListener: (Employees) -> Unit
) : RecyclerView.Adapter<AttendanceEmployeeListAdapter.ViewHolder>() {

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AttendanceEmployeeListAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_empllist, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: AttendanceEmployeeListAdapter.ViewHolder, position: Int) {
        holder.bindItems(employeeList[position], RightclickListener,LeftclickListener)

    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return employeeList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(employeeList: Employees, RightclickListener: (Employees) -> Unit, LeftclickListener: (Employees) -> Unit) = with(itemView) {
            itemView.txtname.text = employeeList.name
                itemView.imgright.setOnClickListener { RightclickListener(employeeList) }
                itemView.imgclose.setOnClickListener { LeftclickListener(employeeList) }
            }

        }


    }


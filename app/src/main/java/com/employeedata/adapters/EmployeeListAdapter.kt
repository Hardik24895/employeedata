package com.employeedata.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.employeedata.R
import com.employeedata.modal.Employees
import kotlinx.android.synthetic.main.row_employee.view.*


class EmployeeListAdapter(
    val employeeList: ArrayList<Employees>
    ,var callclick: (Employees) -> Unit
    ,var reportclick: (Employees) -> Unit
) : RecyclerView.Adapter<EmployeeListAdapter.ViewHolder>() {

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmployeeListAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_employee, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: EmployeeListAdapter.ViewHolder, position: Int) {
        holder.bindItems(employeeList[position],callclick,reportclick)


    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return employeeList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(employeeList: Employees, callclick: (Employees) -> Unit, reportclick: (Employees) -> Unit) = with(itemView) {
            itemView.txtname.text = employeeList.name
            itemView.imgcall.setOnClickListener { callclick(employeeList)  }
            itemView.imgreport.setOnClickListener { reportclick(employeeList)  }
            itemView.txtname.setOnClickListener { reportclick(employeeList)}
            }


    }

}
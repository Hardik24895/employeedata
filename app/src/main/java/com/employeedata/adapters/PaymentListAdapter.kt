package com.employeedata.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.employeedata.R
import com.employeedata.modal.Payment
import com.employeedata.utility.constant
import kotlinx.android.synthetic.main.row_paymentlist.view.*


class PaymentListAdapter(
    val paymentList: ArrayList<Payment>

) : RecyclerView.Adapter<PaymentListAdapter.ViewHolder>() {

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentListAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_paymentlist, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: PaymentListAdapter.ViewHolder, position: Int) {
        holder.bindItems(paymentList[position])


    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return paymentList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(paymentlist: Payment) = with(itemView) {
            itemView.txtamount.text = paymentlist.amount.toString()
            itemView.txtdate.text = constant.getDateTime(paymentlist.date!!)

        }


    }

}
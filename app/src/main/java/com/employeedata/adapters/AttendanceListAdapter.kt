package com.employeedata.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.employeedata.R
import com.employeedata.modal.Attendance
import com.employeedata.utility.constant.getDateTime
import kotlinx.android.synthetic.main.row_attendancelist.view.*
import java.text.SimpleDateFormat
import java.util.*


class AttendanceListAdapter (
    val attendanceList: ArrayList<Attendance>

) : RecyclerView.Adapter<AttendanceListAdapter.ViewHolder>() {

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AttendanceListAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_attendancelist, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: AttendanceListAdapter.ViewHolder, position: Int) {
        holder.bindItems(attendanceList[position])


    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return attendanceList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(attendance: Attendance) = with(itemView) {
            itemView.txtdate.text = getDateTime(attendance.date!!)



        }



    }


}
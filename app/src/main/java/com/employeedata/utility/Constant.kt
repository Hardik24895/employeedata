package com.employeedata.utility

import android.content.Context
import android.content.SharedPreferences
import android.view.View
import java.text.SimpleDateFormat
import java.util.*

object constant {

    var USER_KEY:String =""
    public fun getDateTime(s: String): String? {
        try {
            val sdf = SimpleDateFormat("dd/MM/yyyy")
            val netDate = Date(s.toLong())
            return sdf.format(netDate)
        } catch (e: Exception) {
            return e.toString()
        }
    }

    private var preferences: SharedPreferences? = null
    private var editor: SharedPreferences.Editor? = null
    internal lateinit var mContext: Context

    fun PreferenceManager(mContext: Context) {
        this.mContext = mContext
        preferences = android.preference.PreferenceManager.getDefaultSharedPreferences(mContext)
        editor = preferences!!.edit()
    }

    fun setBooleanPreference(key: String, booleanValue: Boolean) {
        editor!!.putBoolean(key, booleanValue).apply()
    }

    fun setStringPreference(key: String, stringvalue: String) {
        editor!!.putString(key, stringvalue).apply()
    }

    fun getStringPreference(key: String):String{
        return preferences!!.getString(key,"")
    }

    fun getBooleanPreference(key: String): Boolean {
        return preferences!!.getBoolean(key, false)
    }

    fun clearPrefrences() {
        editor!!.clear()
        editor!!.apply()
    }


}
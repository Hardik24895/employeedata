package com.employeedata.utility

import android.content.Context
import android.content.SharedPreferences

class PreferenceManager {
    private var preferences: SharedPreferences? = null
    private var editor: SharedPreferences.Editor? = null
    internal lateinit var mContext: Context

    fun PreferenceManager(mContext: Context) {
        this.mContext = mContext
        preferences = android.preference.PreferenceManager.getDefaultSharedPreferences(mContext)
        editor = preferences!!.edit()
    }

    fun setBooleanPreference(key: String, booleanValue: Boolean) {
        editor!!.putBoolean(key, booleanValue).apply()
    }

    fun getBooleanPreference(key: String): Boolean {
        return preferences!!.getBoolean(key, false)
    }

    fun clearPrefrences() {
        editor!!.clear()
        editor!!.apply()
    }

}